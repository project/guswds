const devConfig = {
  extends: '.eslintrc.cjs',
  rules: {
    'no-console': 'off',
    'no-empty-function': 'off',
    'no-unused-vars': 'off',
    'prefer-const': 'off',
  },
};

module.exports = devConfig;
